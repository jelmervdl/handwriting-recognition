from __future__ import division
from dataset import Dataset
import cv2
import numpy as np
import matplotlib.pyplot as plt
from toolbox import pamImage, cocoslib
import random

def slice_len(slice):
    return slice.stop - slice.start

def mask_to_PamImage(data):
    image = pamImage.PamImage(pamImage.BW_IMAGE, data.shape[1], data.shape[0])
    for y in range(data.shape[0]):
        for x in range(data.shape[1]):
            image.putPixel(x, y, int(data[y][x]))
    return image

def extract(word, page):
    return np.copy(page.image[word.slice])

def estimate_bounds(word, page):
    assert all(slice_len(dim) > 0 for dim in word.slice), "Area of {} is smaller or equal to 0.".format(word)
    # Invert the mask again
    mask = np.copy(page.mask[word.slice])
    cv2.bitwise_not(mask, mask)

    # Create a PamImage from the mask
    try:
        pam_image = mask_to_PamImage(mask)
    except:
        print("Mask size: {}".format(mask.shape))
        print("Page size: {}".format(page.mask.shape))
        print("Word slice: {}".format(word.slice))
        raise

    # Use cocos to find the connected components
    cocos = cocoslib.Cocos(pam_image, 8, 0)

    n_components = cocos.getNum()

    if n_components < 1:
        return None

    b_left, b_top, b_right, b_bottom = cocos.getCocoRect(0)
    for i in range(1, n_components):
        left, top, right, bottom = cocos.getCocoRect(i)
        if min(left, right) < b_left:
            b_left = min(left, right)
        if min(top, bottom) < b_top:
            b_top = min(top, bottom)
        if max(left, right) > b_right:
            b_right = max(left, right)
        if max(top, bottom) > b_bottom:
            b_bottom = max(top, bottom)

    return (slice(b_top, b_bottom), slice(b_left, b_right))


if __name__ == '__main__':
    dataset = Dataset('./data/pages/KNMP/','./data/charannotations/KNMP/')

    pages = list(dataset.pages)

    page = pages[1]

    characters = list(page.characters)

    character, word = random.choice(characters)

    print(word)

    bounds = estimate_bounds(word, page)

    # # # cv2.rectangle
    cv2.imshow("Word", page.image[word.slice])
    cv2.imshow("Bounds", page.image[word.slice][bounds])
    # # cv2.imshow("result", response)
    cv2.waitKey()
    cv2.destroyAllWindows()