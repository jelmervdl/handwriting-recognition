from recognizer import feature_vector, FeatureExtractionError
from dataset import default_dataset, split_pages
from sklearn import svm
import numpy as np
import test_widths

widths = dict((char, np.mean(widths)) for char, widths in test_widths.widths.items())

if __name__ == '__main__':
    train_pages, test_pages = split_pages(default_dataset())

    train_labels = list()
    train_samples = list()

    label_index = list(widths.keys())

    for page in train_pages:
        with page:
            for word in page.words:
                for character in word.characters:
                    try:
                        letter = character.text.lower()
                        if not letter.isalpha():
                            continue
                        
                        padding = int((widths[letter] - character.shape[0]) / 2)
                        character.left -= padding
                        character.right += padding

                        fvec, shape = feature_vector(character, page)
                        
                        train_labels.append(label_index.index(letter))
                        train_samples.append(fvec)
                    except FeatureExtractionError:
                        pass

    my_svm = svm.SVC(probability=True, C=100.0, gamma=1.0e-09) 
    my_svm.fit(np.array(train_samples), np.array(train_labels))

    n = 0
    n_correct = 0
    n_total = sum(len(list(page.characters)) for page in test_pages)

    for page in test_pages:
        with page:
            for word in page.words:
                for character in word.characters:
                    try:
                        letter = character.text.lower()
                        if not letter.isalpha():
                            continue

                        padding = int((widths[letter] - character.shape[0]) / 2)
                        character.left -= padding
                        character.right += padding

                        fvec, shape = feature_vector(character, page)

                        predicted_letter_idx = my_svm.predict(np.array([fvec]))
                        
                        correct = label_index[predicted_letter_idx] == letter

                        n += 1
                        if correct:
                            n_correct += 1
                    except FeatureExtractionError:
                        pass

                print("{:0.2f}% done; {:0.2f}% correct".format(100 * n / n_total, 100 * n_correct / n))

