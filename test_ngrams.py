from dataset import Dataset, CombinedDataset, ngrams
from collections import Counter
import re

def clean_text(text):
    return re.sub('(@[a-z]+_)|[^a-z0-9]', '', text.lower())

def bigramlist():

    dataset = CombinedDataset([
        Dataset('./data/pages/KNMP/','./data/charannotations/KNMP/'),
        Dataset('./data/pages/Stanford/','./data/charannotations/Stanford/')
    ])

    counter = Counter()

    for n in range(2, 3):
        for word, page in dataset.words:
            for ngram in ngrams([character.text for character in word.characters if character.text.isalpha()], n):
                counter[ngram] += 1

    bigrams = []

    for ngram, occurrences in counter.most_common(100):
     	# print("{}\t\t{}".format("".join(ngram), occurrences))
        bigrams.append("".join(ngram))

    return bigrams

if __name__ == '__main__':
    bigramlist()