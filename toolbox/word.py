class Entity:
	top = None
	left = None
	bottom = None
	right = None
	shear = None
	text = None
	confidence = None

	@property
	def shape(self):
		return (self.right - self.left, self.bottom - self.top)

	@property
	def slice(self):
		return (slice(self.top, self.bottom),slice(self.left,self.right))

	def __tuple(self):
		return (self.top, self.left, self.bottom, self.right, self.shear, self.text)

	def __hash__(self):
		return hash(self.__tuple())

	def __eq__(self, other):
		return self.__tuple() == other.__tuple()

	def __ne__(self, other):
		return not (self == other)

	def __repr__(self):
		return '%s(top=%d, left=%d, bottom=%d, right=%d, shear=%s, text=%r)' % ((self.__class__.__name__,) + self.__tuple())

	

class Character(Entity):
	pass


class Word(Entity):
	def __init__(self):
		self.characters = []
	
