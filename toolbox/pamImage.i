%module pamImage

%inline %{
	#include <iostream>
	#include <streambuf>
	#include <sstream>

	struct cerr_redirect {
	    cerr_redirect( std::streambuf * new_buffer ) 
	        : old( std::cerr.rdbuf( new_buffer ) )
	    { }

	    ~cerr_redirect( ) {
	        std::cerr.rdbuf( old );
	    }

	private:
	    std::streambuf * old;
	};
%}

%exception {
	std::stringstream errbuffer;
	cerr_redirect redirect(errbuffer.rdbuf());
	try {
		$action
	} catch (...) {
		PyErr_SetString(PyExc_RuntimeError, errbuffer.str().c_str());
		return NULL;
	}
}

%include "std_string.i"
%{
#include "pamImage.h"
%}

%include "pamImage.h"
