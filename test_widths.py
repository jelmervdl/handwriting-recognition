from __future__ import division
from dataset import Dataset, CombinedDataset, ngrams, default_dataset
from collections import defaultdict
import matplotlib.pyplot as plt
import re
from math import ceil
from itertools import izip_longest
import sys
import test_ngrams
import numpy as np


def clean_text(text):
    return re.sub('/(@[a-z]_)|[^a-z0-9]/', '', text.lower())

def gaussian_bigrams():
    ngrams = test_ngrams.bigramlist()

    dataset = default_dataset()

    widths = defaultdict(list)

    # for word, page in dataset.words:
    #     for character in word.characters:
    #         if character.text.isalpha():
    #             widths[character.text.lower()].append(character.right - character.left)

    for word, page in dataset.words:
        if word.text.isalpha():
            for bigram in ngrams:
                pos = word.text.find(bigram)
                if pos >= 0 and len(word.characters) > pos + 1:
                    # print word.text + ' ' + bigram + ' ' + str(pos) + ' ' + word.characters[pos + 1].text 
                    widths[bigram].append(word.characters[pos + 1].right - word.characters[pos].left)

    print len(widths)

    for item in widths.keys():
        if np.std(widths[item]) > 8:
            del widths[item]

    return widths


if __name__ == '__main__':
    widths = gaussian_bigrams()

    fig, axes = plt.subplots(ncols=4, nrows=int(ceil(len(widths)/4)), figsize=(10, 10))

    fig.subplots_adjust(hspace=1.75, wspace=0.25)

    for ax, char_width in izip_longest(axes.flat, sorted(widths.items(), key=lambda item: item[0])):
        if char_width is not None:
            ax.hist(char_width[1], 80, range=(20, 120))
            ax.set_title('"{}"'.format(char_width[0]))
            ax.get_xaxis().set_ticks([0, 20, 40, 60, 80])
            ax.get_yaxis().set_ticks([])
        else:
            ax.axis('off')

    if len(sys.argv) == 2:
        plt.savefig(sys.argv[1])
    else:
        plt.show()
