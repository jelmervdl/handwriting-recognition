#!/usr/bin/env python
# coding=utf-8
from __future__ import division, unicode_literals
from toolbox.word import Character, Word
from toolbox import wordio
from dataset import Page, default_dataset, split_pages
from collections import defaultdict
from random import shuffle
import cv2
import numpy as np
from features import hog
from test_mask import extract
from test_bounding_box import estimate_bounds
from lexicon import load as load_lexicon, extract_from_pages as lexicon_from_pages
from sklearn import svm
from copy import copy
import os
import sys
import pickle
import json
import operator
import gzip
import re
import lexicon
import itertools

state_path = "state.p"

lexicon_path = "data/hwr_course_lexicon_testset.txt"

hog_bins = 128 # 71.77

DUMP_PARTIALS = False

MAX_PARTIAL_COUNT = 5000

def shuffled(values):
    shuffled_values = list(values)
    shuffle(shuffled_values)
    return shuffled_values


def ordinal(n):
    return "{:d}{:s}".format(n, {1:"st",2:"nd",3:"rd"}.get(n if n < 20 else n % 10, "th"))


class FeatureExtractionError(Exception):
    def __init__(self, element):
        self.element = element
    

class EmptyImageError(FeatureExtractionError):
    def __str__(self):
        return "Empty image for element: {}".format(self.element)


class InvalidElementError(FeatureExtractionError):
    def __str__(self):
        return "Wrongly annotated element: {}".format(element)


class ElementOutOfPageError(FeatureExtractionError):
    def __init__(self, element, page):
        self.element = element
        self.page = page

    def __str__(self):
        return "Element ({}) is (partially) outside the page bounds ({}).".format(self.element.slice, self.page.shape)


class MaxPartialTriesError(Exception):
    def __init__(self, tries):
        self.tries = tries

    def __str__(self):
        return "Reached the partial limit when trying the {} partial".format(ordinal(self.tries))


def feature_vector(element, page):
    if element.left >= element.right or element.top >= element.bottom:
        raise InvalidElementError(element)

    if element.right > page.width \
     or element.bottom > page.height \
     or element.left < 0 \
     or element.top < 0:
        raise ElementOutOfPageError(element, page)

    element_image = extract(element, page)
    bounds = estimate_bounds(element, page)
    if bounds is not None:
        element_image = element_image[bounds]
    
    if element_image.size == 0:
        raise EmptyImageError(element)

     # Create a description of the image using the HOG descriptor
    description = hog(element_image, bin_n=hog_bins)

    feature_vector = np.float32(np.concatenate([
        description.reshape(4 * hog_bins),
        # TODO: Maybe this doenst work because the range of this value
        # is of a completely different order of magnitude compared to the
        # HOG descriptor 
        #[(element_image.shape[1] / element_image.shape[0])] # decreases score by ~2%?
    ], axis=0))

    return feature_vector, element_image.shape


class NormalDistribution:
    def __init__(self, mean, sd):
        self.mean = mean
        self.sd = sd

    def __contains__(self, value):
        return self.contains(value)

    @classmethod
    def from_values(cls, values):
        return cls(np.mean(values), np.std(values))
    
    def contains(self, value):
        return np.abs(self.zscore(value)) <= 2

    def zscore(self, value):
        return (value - self.mean) / self.sd


class Partial:
    def __init__(self, word, left, right):
        self._word = word
        self.left = left
        self.right = right

    def next(self, left=[], right=[]):
        return Partial(self._word, self.left + left, right + self.right)

    def in_vocabulary(self, vocabulary):
        for word in vocabulary.keys():
            if len(word) >= len(self.left) + len(self.right) \
             and word.startswith(self.left_text) \
             and word.endswith(self.right_text):
                return True
        return False

    def is_word(self, vocabulary):
        return self.word.text in vocabulary.keys()

    def is_sufficient(self, ratio=0.85):
        return self.right_cursor - self.left_cursor < (1 - ratio) * self._word.shape[0]

    def is_complete(self):
        return self.right_cursor - self.left_cursor < 0

    def predict_left(self, characters, lexicon):
        """Predict the next possible characters"""
        predicted = list()

        for char in characters:
            partial_left = self.left_text + char
            partial_right = self.right_text
            for word in lexicon.keys():
                if len(word) >= len(partial_left) + len(partial_right) \
                 and word.startswith(partial_left) \
                 and word.endswith(partial_right):
                    predicted.append(char)
                    break

        return predicted

    def predict_right(self, characters, lexicon):
        """Predict the next possible characters"""
        predicted = list()

        for char in characters:
            partial_left = self.left_text
            partial_right = char + self.right_text
            for word in lexicon.keys():
                if len(word) >= len(partial_left) + len(partial_right) \
                 and word.startswith(partial_left) \
                 and word.endswith(partial_right):
                    predicted.append(char)
                    break

        return predicted

    @property
    def left_cursor(self):
        return max([self._word.left] + [char.right for char in self.left])

    @property
    def right_cursor(self):
        return min([self._word.right] + [char.left for char in self.right])

    @property
    def left_text(self):
        return "".join(char.text for char in self.left)

    @property
    def right_text(self):
        return "".join(char.text for char in self.right)

    @property
    def overlap(self):
        '''Positive when the right bound has passed the left bound'''
        return self.left_cursor - self.right_cursor

    @property
    def key(self):
        return "{partial.left_text}|{partial.right_text}".format(partial=self)
    
    @property
    def word(self):
        word = copy(self._word)
        word.confidence = self.confidence
        word.characters = self.left + self.right
        word.text = "".join(char.text for char in self.left + self.right)
        return word

    @property
    def confidence(self):
        # TODO: Better combined confidence. The correct way to do this would be
        # by multiplying all character confidences, but that would cause longer
        # words to always be at a disadvantage.
        assert len(self.left + self.right), "Partials without any characters have no confidence yet."
        # return np.mean([char.confidence for char in self.left + self.right])
        return reduce(operator.mul, [char.confidence for char in self.left + self.right])

    def __len__(self):
        return len(self.left) + len(self.right)

    def __repr__(self):
        return "{word_left}|{char_left}|{cur_left}..{cur_right}|{char_right}|{word_right}".format(
                char_left="".join(["[{}]".format(char.text) for char in self.left]),
                char_right="".join(["[{}]".format(char.text) for char in self.right]),
                cur_left=self.left_cursor,
                cur_right=self.right_cursor,
                word_left=self._word.left,
                word_right=self._word.right)


class Recognizer:
    def train(self, pages, C=100.0, gamma=1.0e-09, **svm_args):
        """
        Train the SVM and determines the window widths we are scanning for.
        """

        n_total = 0

        # First determine the prototypical character widths
        character_widths = defaultdict(list)

        for page in pages:
            for word in page.words:
                for character in word.characters:
                    if not character.text.isalpha():
                        continue
                    n_total += 1
                    character_widths[character.text].append(character.shape[0])

        self.character_widths = dict((character, NormalDistribution.from_values(widths)) for character, widths in character_widths.items())

        print("There are {} characters and {} labels to train on in total".format(n_total, len(character_widths)))

        for character, widths in character_widths.items():
            print("{}\t{} samples\tM:{}\tsd:{}".format(character, len(widths), np.mean(widths), np.std(widths)))

        # Next, train the characters
        samples = list()
        labels = list()
        n = 0
        n_error = 0
        for page in pages:
            with page:
                for word in page.words:
                    for character in word.characters:
                        # Skip all non alphanumeric characters
                        if not character.text.isalpha():
                            continue

                        n += 1

                        # Make the size of the character prototypical
                        padding = int((self.character_widths[character.text].mean - character.shape[0]) / 2)
                        character.left -= padding
                        character.right += padding

                        try:
                            fvec, _ = feature_vector(character, page)
                            samples.append(fvec)
                            labels.append(character.text)
                        except FeatureExtractionError:
                            n_error += 1
                            pass

                        if n % 100 == 0:
                            print("Sampling {} of {} ({:0.2f}%, {} errors)".format(n, n_total, 100 * n / n_total, n_error))
        
        # Create a sort-of mapping of int->character because the SVM works with ints.
        self.characters = list(np.unique(labels))

        # Fit the SVM
        print("Fitting SVM...")
        self.svm = svm.SVC(probability=True, C=C, gamma=gamma, **svm_args) 
        numeric_labels = [self.characters.index(character) for character in labels]
        self.svm.fit(np.array(samples), np.array(numeric_labels))


    def test_characters(self, pages):
        '''Test the trained recognizer on characters only.'''
        n_correct = 0
        n_total = 0
        n_error = 0
        for page in pages:
            with page:
                for character, word in page.characters:
                    if not character.text.isalpha():
                        continue

                    try:
                        padding = int((self.character_widths[character.text].mean - character.shape[0]) / 2)

                        character = copy(character)
                        character.left -= padding
                        character.right += padding
                        
                        solutions = self.recognize_character(character, page)

                        n_total += 1

                        if len(solutions) > 0:
                            best_solition = max(solutions, key=lambda pair: pair[1])
                            if best_solition[0].lower() == character.text.lower():
                                n_correct += 1
                    except FeatureExtractionError, cv2.error:
                        n_error += 1
                        pass
        return n_correct, n_error, n_total


    def recognize_word(self, word, page, lexicon):
        """
        Recognize the characters in a Word object. The Word object needs to
        contain boundaries (top, left, bottom, right).

        This method returns a list of options, sorted by their probability,
        with the best option on top. These options have the properties
        'confidence', which represents the confidence the recognizer has in them
        and 'word', which again yields a Word object but now with the correct
        text and characters.
        """

        # Work on a page that is completely blank except for the word,
        # so that even when we go out of bounds, we ignore the next words
        masked_page = copy(page)
        masked_page.image = np.zeros(shape=page.image.shape, dtype=page.image.dtype)
        masked_page.image[word.slice] = page.image[word.slice]
        masked_page.mask = np.zeros(shape=page.mask.shape, dtype=page.mask.dtype)
        masked_page.mask[word.slice] = page.mask[word.slice]
        page = masked_page

        todo = [Partial(word, [], [])]

        complete = []

        partial_count = 0

        recognized_slice_cache = dict()

        # Keep on repeating this loop while there are still Partial options open
        try:
            while len(todo) > 0:
                partial_count += 1

                if partial_count > MAX_PARTIAL_COUNT:
                    raise MaxPartialTriesError(partial_count)
                
                partial = todo.pop()

                # If a partial has been sufficiently recognized (i.e. we recognized at least
                # until 85% of the width of the word), mark it as complete enough to be a solution.
                if partial.is_sufficient():
                    if partial.is_word(lexicon):
                        complete.append(partial)

                # Skip further options for this partial if the partial is already long enough.
                # (I.e. the recognized characters together are at least as width as the word.)
                # TODO: This assumes that the provided word object is formed tightly around the
                # actual word. Maybe we need to trim the dimensions a bit using page.mask?
                if partial.is_complete():
                    continue

                # Let the SVM determine the next likely character at the offset of the cursor.
                # We try the same operation, but in both directions (left-to-right and right-to-left)
                for direction in ['ltr', 'rtl']:
                    branch = dict()
                    
                    if direction == 'ltr':
                        predicted_labels = partial.predict_left(self.characters, lexicon)
                    else:
                        predicted_labels = partial.predict_right(self.characters, lexicon)

                    for label in predicted_labels:
                        # Take the average character width (which we trained on anyway)
                        for offset in [0.5 * -self.character_widths[label].sd, 0, 0.5 * self.character_widths[label].sd]:
                            # We also trained on the characters with their mean width as width, and keeping it
                            # consistent between training and testing does increase performance.
                            width = self.character_widths[label].mean

                            # Create a sort of template for the character we expect to find
                            character = Character()
                            character.text = '?'
                            character.top = word.top
                            character.bottom = word.bottom

                            if direction == 'ltr':
                                character.left = partial.left_cursor + offset
                                character.right = partial.left_cursor + offset + width
                                test_partial = partial.next(left=[character])
                            else:
                                character.right = partial.right_cursor - offset
                                character.left = partial.right_cursor - offset - width
                                test_partial = partial.next(right=[character])

                            #print("Test partial {!r}".format(test_partial))

                            if test_partial.overlap > 0.2 * character.shape[0]:
                                #print("Too much overlap: {} (limit {})".format(test_partial.overlap, 0.25 * character.shape[0]))
                                continue

                            try:
                                # First check whether we tried this exact slice before. If so, the confidences
                                # per label on that particular slice can be reused because these don't change!
                                if (character.left, character.right) in recognized_slice_cache:
                                    character_confidences = recognized_slice_cache[(character.left, character.right)]
                                else:
                                    character_confidences = self.recognize_character(character, page)
                                    recognized_slice_cache[(character.left, character.right)] = character_confidences
                                
                                # Determine how confident we are about any of the letters for this width 
                                for text, confidence in character_confidences:
                                    # Only look at characters we expect
                                    if text not in predicted_labels:
                                        continue

                                    # Update the character template with the actual recognized letter
                                    recognized_character = copy(character)
                                    recognized_character.text = text
                                    recognized_character.confidence = confidence

                                    # TODO: We could also add the likelihood that this character is found
                                    # with this window width to the confidence metric. We store all normal
                                    # distributions of the typical sizes of windows in self.character_widths.
                                    # TODO: We even have the apriori chance of finding this character based
                                    # on the lexicon, which we could use. The same for n-grams.
                                    if text in branch.keys():
                                        if confidence > branch[text].confidence:
                                            branch[text] = recognized_character
                                    else:
                                        branch[text] = recognized_character
                            except FeatureExtractionError as e:
                                print("Notice: feature extraction error: {}".format(e))

                    if len(partial) == 0:
                        # First letter: use the mean
                        
                        # What is the mean of our letter confidences?
                        assert len(branch.values()) > 0
                        mean = np.mean([character.confidence for character in branch.values()])
                        
                        # Which of the characters are we more confident about?
                        certain = filter(lambda character: character.confidence > mean, branch.values())

                        if word.text != "" and not any([word.text[0] == character.text for character in certain]):
                            print("Oh god even the first character was not guessed: {} vs a mean of {}".format(
                                branch[word.text[0]].confidence if word.text[0] in branch else -1,
                                mean))
                    else:
                        # Next letters: accept all options that can occur according to the lexicon
                        if direction == 'ltr':
                            certain = filter(lambda character: partial.next(left=[character]).in_vocabulary(lexicon), branch.values())
                        else:
                            certain = filter(lambda character: partial.next(right=[character]).in_vocabulary(lexicon), branch.values())

                    # Sort them on confidence
                    #certain = sorted(certain, key=lambda character: character.confidence, reverse=True)

                    # Convert all those to partials and add them to the todo list
                    if direction == 'ltr':
                        new_partials = (partial.next(left=[character]) for character in certain)
                    else:
                        new_partials = (partial.next(right=[character]) for character in certain)

                    # filter out any partials that don't make the confidence threshold
                    new_partials = filter(lambda partial: partial.confidence > 1.0e-4, new_partials)

                    # Update the todo list
                    todo = self.merge_partials(todo + new_partials)

        except MaxPartialTriesError as e:
            print("Warning: " + str(e))
        except:
            print("\nStopped while trying to guess {}".format(word.text))
            print("Evaluated {:d} partials".format(partial_count))
            print("Todo list:")
            for partial in todo:
                print("{p!r} ({p.confidence:f})".format(p=partial))
            print("Complete list:")
            for partial in complete:
                print("{p!r} ({p.confidence:f})".format(p=partial))
            raise
        
        # Convert the partials to complete Word objects
        complete_words = (partial.word for partial in complete)

        # After trying *all* the options, return all of them, but sorted from best to worst
        return sorted(complete_words, key=lambda partial: partial.confidence, reverse=True)

    def merge_partials(self, partials):
        out = dict()
        for partial in partials:
            key = partial.key
            if key in out:
                if out[key].confidence < partial.confidence:
                    out[key] = partial
            else:
                out[key] = partial
        return sorted(out.values(), key=lambda partial: partial.confidence, reverse=True)


    def recognize_character(self, character, page):
        fvec, _ = feature_vector(character, page)
        scores = self.svm.predict_proba(np.array([fvec]))
        return [(self.characters[idx], scores[0,idx]) for idx in range(scores.shape[1])]


if __name__ == '__main__':
    if len(sys.argv) <= 3:
        dataset = default_dataset()

        train_pages, test_pages = split_pages(dataset)
    
        if os.path.exists(state_path):
            with gzip.open(state_path, 'rb') as f:
                recognizer = pickle.load(f)
        else:
            recognizer = Recognizer()

            print("Training...")
            recognizer.train(train_pages)
            
            print("Dumping trained recognizer to {}".format(state_path))
            with gzip.open(state_path, 'wb') as f:
                pickle.dump(recognizer, f)

            print("Testing on characters...")
            correct, error, total = recognizer.test_characters(test_pages)
            print("{} of {} correct ({} caused errors)".format(correct, total, error))

        print("Testing on words...")

        # If we are dumping the partials for debugging purposes, make sure
        # that directory is cleaned beforehand.
        if DUMP_PARTIALS:
            for filename in os.listdir("dumps"):
                if filename.endswith('.pz'):
                    os.remove("dumps/{}".format(filename))

        # Extract the lexicon from the test set
        lexicon = lexicon_from_pages(test_pages)

        n = 0
        n_correct = 0
        n_total = sum(len(page) for page in test_pages)
        for page in shuffled(test_pages):
            for word in shuffled(page.words):
                n += 1
                
                # Skip words that are only special (ignored) characters
                if len(word.text) == 0:
                    continue

                print("Trying to guess '{}'".format(word.text))
                if word.text not in lexicon.keys():
                    print("  Warning: '{}' not in lexicon! Adding it for now...".format(word.text))
                    lexicon[word.text] = 1
                try:
                    solutions = recognizer.recognize_word(word, page, lexicon)

                    # For debugging purpose, also dump these values for the original cutting work
                    for character in word.characters:
                        try:
                            branch = dict(recognizer.recognize_character(character, page))
                            character.confidence = branch[character.text] if character.text in branch else None
                        except:
                            pass

                    if len(solutions) > 0:
                        print("  Found {} possible solutions".format(len(solutions)))
                        best = solutions[0]
                        correct = best.text.lower() == word.text.lower()
                        print("{} Found '{}' ({})".format("\u2713" if correct else "\u274C", best.text, best.confidence).encode('utf8'))
                        if correct:
                            n_correct += 1
                        else:
                            found = False
                            for idx, solution in enumerate(solutions):
                                if solution.text.lower() == word.text.lower():
                                    print("  Correct solution was found as the {} best option ({})".format(ordinal(idx + 1), solution.confidence))
                                    found = True
                                    break
                            if not found:
                                print("  The correct solution was not among the options 😭")
                        if DUMP_PARTIALS:
                            dump_path = 'dumps/{}_{}_{}.pz'.format(n, word.text, 'hit' if correct else 'miss')
                            with gzip.open(dump_path, 'wb') as f:
                                page_copy = copy(page)
                                page_copy.unload()
                                pickle.dump(dict(page=page_copy, word=word, solutions=solutions), f)
                            print("  dumped in {}".format(dump_path))
                    else:
                        print("\u2757 No solutions found".encode('utf8')) 
                except:
                    raise
                print('~{:0.2f}% done; {:0.2f}% correct'.format(100 * n / n_total, 100 * n_correct / n))
            page.unload()

    if len(sys.argv) > 3:
        # Load the trained recognizer
        if os.path.exists(state_path):
            with gzip.open(state_path, 'rb') as f:
                recognizer = pickle.load(f)
        else:
            raise Exception("Missing the trained recognizer file, '{}'".format(state_path))

        # Load the given test set lexicon
        if os.path.exists(lexicon_path):
            with open(lexicon_path, 'r') as f:
                lexicon = load_lexicon(f)
        else:
            raise Exception("Missing the test set lexicon file, '{}'".format(lexicon_path))


        image_filename = sys.argv[1]
        word_filename = sys.argv[2]
        output_filename = sys.argv[3]
            
        if all(os.path.isdir(filename) for filename in [image_filename, word_filename, output_filename]):
            image_files = [os.path.join(image_filename, filename) for filename in os.listdir(image_filename) if os.path.splitext(filename)[1] in ['.ppm', '.jpg']]
            word_files = [os.path.join(word_filename, filename) for filename in os.listdir(word_filename) if os.path.splitext(filename)[1] in ['.xml', '.words']]
            output_files = [os.path.join(output_filename, os.path.basename(filename)) for filename in word_files]
        elif all(os.path.isfile(filename) for filename in [image_filename, word_filename]):
            image_files = [image_filename]
            word_files = [word_filename]
            output_files = [output_filename]
        else:
            print("""
                Usage: {} image-file word-file output-file
                Note: You can either point to three directory paths or three regular files.
            """.format(sys.argv[0]))
            sys.exit(2)

        assert len(image_files) == len(word_files), "Unequal number of image and word files"

        image_files.sort()
        word_files.sort()
        output_files.sort()

        for page_idx, image_filename, word_filename, output_filename in zip(itertools.count(), image_files, word_files, output_files):
            print("File {} out of {}\n  image: {}\n  words: {}\n  output: {}".format(
                page_idx + 1, len(word_files), image_filename, word_filename, output_filename))

            lines, _ = wordio.read(word_filename)
            
            page = Page(lines, image_filename, word_filename)

            n_tried = 0
            n_recognized = 0
            n_total = len(page)

            for word in page.words:
                if len(word.characters) > 0:
                    print("Skipping already recognized {}".format(word))
                    continue
                
                n_tried += 1
                print("Progress: {n_tried} of {n_total}: {percentage:0.2f}% ({n_recognized} recognized)".format(
                    n_tried=n_tried, n_total=n_total, n_recognized=n_recognized, percentage=100*n_tried/n_total))

                solutions = recognizer.recognize_word(word, page, lexicon)
                if len(solutions) > 0:
                    recognized_word = solutions[0]
                    word.text = recognized_word.text
                    word.confidence = recognized_word.confidence
                    word.characters = recognized_word.characters
                    n_recognized += 1

                wordio.save(page.lines, output_filename)
