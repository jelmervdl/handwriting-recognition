from __future__ import division

from dataset import Dataset, CombinedDataset, Lexicon
from collections import Counter, defaultdict
from flask import Flask, send_file, Response, abort
import numpy as np
import cv2
import sys
import base64
import urllib
from test_mask import extract
from test_bounding_box import estimate_bounds

def imgtobase64(img):
    buffer = cv2.imencode('.jpg', img)[1]
    return "data:image/jpeg;base64," + base64.encodestring(buffer)

def webservice(dataset, lexicon=None):
    app = Flask(__name__)
    app.debug = True

    # Fall back to an empty defaultdict that always returns 0 if no lexicon is provided
    if lexicon is None:
        lexicon = defaultdict(int)

    # Just load all the pages once and then keep this list as index
    pages = list(dataset.pages)

    # Make an index of where every word and every character occurs
    word_index = defaultdict(list)
    character_index = defaultdict(list)
    for page in pages:
        for word in page.words:
            word_index[word.text].append((word, page))
            for character in word.characters:
                character_index[character.text].append((character, page))

    @app.route('/')
    def app_index():
        links = list()
        for i, page in enumerate(pages):
            links.append('''
                <tr>
                    <th><a href="/page/{page_number}">{page_name}</a></th>
                    <td>{word_count}</td>
                    <td>{character_count}</td>
                </tr>
            '''.format(
                page_number=i,
                page_name=str(page),
                word_count=sum(1 for word in page.words),
                character_count=sum(1 for char in page.characters)))
        return '''
            <style>
            html, body {{
                font: 12px/16px sans-serif;
            }}
            </style>
            <a href="/words/">Words</a>
            <a href="/characters/">Characters</a>
            <table>
                <thead>
                    <tr>
                        <th>Page</th>
                        <th>Words</th>
                        <th>Characters</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        '''.format(rows="\n".join(links))

    @app.route('/words/')
    def app_word_index():
        rows = list()
        for word, occurrences in sorted(word_index.items(), key=lambda x: len(x[1]), reverse=True):
            rows.append('''
                <tr>
                    <th><a href="/words/{permalink}">{word}</a></th>
                    <td>{occurrences}</td>
                    <td>{in_lexicon}</td>
                    <td>{in_testset}</td>
                </tr>
            '''.format(
                    permalink=urllib.quote(word),
                    word=word,
                    occurrences=len(occurrences),
                    in_lexicon=lexicon[word],
                    in_testset=(lexicon[word] - len(occurrences))))

        return '''
            <style>
            html, body {{
                font: 12px/16px sans-serif;
            }}
            th {{
                text-align: left;
            }}
            </style>
            <table>
                <thead>
                    <tr>
                        <th>Word</th>
                        <th>Occurrences</th>
                        <th>In Lexicon</th>
                        <th>In Testset</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        '''.format(rows="\n".join(rows))

    @app.route('/words/<word_txt>')
    def app_word_occurrences(word_txt):
        def generate():
            yield '''
            <style>
            html, body {
                font: 12px/16px sans-serif;
            }
            th {
                text-align: left;
            }
            </style>
            <table>
                <thead>
                    <tr>
                        <th>Word</th>
                        <th>Mask</th>
                        <th>Filtered</th>
                        <th>Page</th>
                        <th>Position</th>
                    </tr>
                </thead>
                <tbody>
            '''
            prev_page = None
            for word, page in word_index[word_txt]:
                if prev_page != page and prev_page is not None:
                    prev_page.unload()
                prev_page = page
                masked_image = extract(word, page)
                bounds = estimate_bounds(word, page)
                if bounds is not None:
                    masked_image = masked_image[bounds]
                yield '''
                <tr>
                    <td><img src="{encoded_image_url}"></td>
                    <td><img src="{encoded_mask_url}"></td>
                    <td><img src="{encoded_masked_image_url}"></td>
                    <td><a href="/page/{page_number}#word{word_hash}">{page}</a></td>
                    <td>{word}</td>
                </tr>
                '''.format(
                    encoded_image_url=imgtobase64(page.image[word.slice]),
                    encoded_mask_url=imgtobase64(page.mask[word.slice]),
                    encoded_masked_image_url=imgtobase64(masked_image),
                    page=page,
                    word=word,
                    word_hash=hash(word),
                    page_number=pages.index(page))
            if prev_page is not None:
                prev_page.unload()
            yield '''
                </tbody>
            </table>
            '''
        return Response(generate(), mimetype='text/html')

    @app.route('/characters/')
    def app_character_index():
        rows = list()
        for character in sorted(character_index.keys()):
            rows.append('''
                <tr>
                    <th><a href="/characters/{permalink}">{character}</a></th>
                    <td>{occurrences}</td>
                </tr>
            '''.format(
                    permalink=urllib.quote(character),
                    character=character,
                    occurrences=len(character_index[character])))

        return '''
            <style>
            html, body {{
                font: 12px/16px sans-serif;
            }}
            th {{
                text-align: left;
            }}
            </style>
            <table>
                <thead>
                    <tr>
                        <th>Character</th>
                        <th>Occurrences</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        '''.format(rows="\n".join(rows))


    @app.route('/characters/<character_txt>')
    def app_character_occurrences(character_txt):
        def generate():
            yield '''
            <style>
            html, body {
                font: 12px/16px sans-serif;
            }
            th {
                text-align: left;
            }
            </style>
            <table>
                <thead>
                    <tr>
                        <th>Character</th>
                        <th>Mask</th>
                        <th>Masked image</th>
                        <th>Page</th>
                        <th>Position</th>
                    </tr>
                </thead>
                <tbody>
            '''
            prev_page = None
            for character, page in character_index[character_txt]:
                if prev_page != page and prev_page is not None:
                    prev_page.unload()
                prev_page = page
                masked_image = extract(character, page)
                bounds = estimate_bounds(character, page)
                if bounds is not None:
                    masked_image = masked_image[bounds]
                yield '''
                <tr>
                    <td><img src="{encoded_image_url}"></td>
                    <td><img src="{encoded_mask_url}"></td>
                    <td><img src="{encoded_masked_image_url}"></td>
                    <td><a href="/page/{page_number}#character{character_hash}">{page}</a></td>
                    <td>{character}</td>
                </tr>
                '''.format(
                    encoded_image_url=imgtobase64(page.image[character.slice]),
                    encoded_mask_url=imgtobase64(page.mask[character.slice]),
                    encoded_masked_image_url=imgtobase64(masked_image),
                    page=page,
                    character=character,
                    character_hash=hash(character),
                    page_number=pages.index(page))
            if prev_page is not None:
                prev_page.unload()
            yield '''
                </tbody>
            </table>
            '''
        return Response(generate(), mimetype='text/html')


    @app.route('/page/<int:page_number>.jpg')
    def app_show_page_image(page_number):
        return send_file(pages[page_number].image_path)

    @app.route('/page/<int:page_number>')
    def app_show_page(page_number):
        page = pages[page_number]

        if not page:
            abort(404)

        # Generate a HTML box for each word with relative positions
        word_template ='''<div title="{text}"
            id="word{hash}"
            class="word"
            style="
                position: absolute;
                top:{top}%;
                left:{left}%;
                width:{width}%;
                height:{height}%;"></div>
        '''
        words_html = (word_template.format(
                hash=hash(word),
                text=word.text,
                top=100 * word.top / page.height,
                left=100 * word.left / page.width,
                height=100 * (word.bottom - word.top) / page.height,
                width=100 * (word.right - word.left) / page.width) for word in page.words)
        # Add it to a pretty page
        html = '''
            <style>
            .word {{
                border: 1px solid blue;
            }}
            .word:hover {{
                background-color: rgba(255, 0, 0, 0.2);
            }}
            .word:target {{
                border-width: 3px;
            }}
            </style>
            <dl>
                <dt>Image</dt>
                <dd>{image_path}</dd>
                <dt>Words</dt>
                <dd>{words_path}</dd>
            </dl>
            <div style="max-width: 100%; position: relative">
                <img src="{image}" width="100%">
                {words_html}
            </div>
        '''.format(
            image=imgtobase64(page.image),
            image_path=page.image_path,
            words_path=page.words_path,
            page_number=page_number,
            words_html="\n".join(words_html))
        # Unload page afterwards to conserve memory (it was loaded because we asked for the dimensions)
        page.unload()
        return html

    return app

if __name__ == '__main__':
    if len(sys.argv) == 3:
        dataset = Dataset(sys.argv[1], sys.argv[2], image_filename_format="{}.ppm")
        lexicon = Lexicon.load('./data/hwr_course_lexicon_testset.txt')
    else:
        dataset = CombinedDataset([
            Dataset('./data/pages/KNMP/','./data/charannotations/KNMP/'),
            Dataset('./data/pages/Stanford/','./data/charannotations/Stanford/')
        ])
        lexicon = Lexicon.load('./data/hwr_course_lexicon_testset.txt')
        #lexicon_dataset = Lexicon.extract(dataset)
        #lexicon_diff = set(lexicon.keys()) - set(lexicon_dataset.keys())

    webservice(dataset, lexicon).run()
