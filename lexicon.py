from collections import defaultdict

def load(fh):
	lexicon = dict()
	for line in fh:
		word, occurrences = line.split(' ')[0:2]
		lexicon[word] = int(occurrences)
	return lexicon

def extract_from_pages(pages):
    lexicon = defaultdict(int)
    for page in pages:
        for word in page.words:
            lexicon[word.text] += 1
    return lexicon