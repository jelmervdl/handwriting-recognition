from __future__ import division
from toolbox import wordio
from dataset import Page
import sys

n_total = 0
n_complete = 0

for filename in sys.argv[1:]:
    lines, _ = wordio.read(filename)
    page = Page(lines, None, filename)

    for word in page.words:
        n_total += 1
        if len(word.text) > 0:
            n_complete += 1

print("{} of {} ({:0.1f}%) complete".format(n_complete, n_total, 100 * n_complete / n_total))

