from __future__ import division
from dataset import Dataset
import cv2
import numpy as np

def extract(element, page, dilate_iterations=3, background_value=0):
    image = np.copy(page.image[element.slice])

    # Take the mask, and add a bit of extra space in the mask around the edges using dilate
    kernel = np.ones((3,3), np.uint8)
    mask = np.copy(page.mask[element.slice])
    mask = cv2.dilate(mask, kernel, iterations=dilate_iterations)

    # Take only the background (image where mask is False) as a masked array
    background = np.ma.array(image, mask=(mask==False))

    # Take the median from only the background as the new background, whiping of all the noise in the background
    image[mask == 0] = background_value if background_value is not None else np.ma.median(background)

    return image


if __name__ == '__main__':
    dataset = Dataset('./data/pages/KNMP/','./data/charannotations/KNMP/')

    pages = list(dataset.pages)

    page = pages[0]

    characters = list(page.characters)

    character, word = characters[0]

    # Can be page, word, character
    image = extract(word, page)

    cv2.imshow("result", image)
    cv2.waitKey()