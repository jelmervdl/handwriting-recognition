#!/usr/bin/python

import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt


def calculate_otsu_thresh(img):
    blur = cv2.GaussianBlur(img,(3,3),0)

    # find normalized_histogram, and its cumulative distribution function
    # hist = cv2.calcHist([blur],[0],None,[256],[0,256])
    # hist_norm = hist.ravel()/hist.max()
    # Q = hist_norm.cumsum()

    # bins = np.arange(256)

    # fn_min = np.inf
    # thresh = -1

    # for i in xrange(1,256):
    #     p1,p2 = np.hsplit(hist_norm,[i]) # probabilities
    #     q1,q2 = Q[i],Q[255]-Q[i] # cum sum of classes
    #     b1,b2 = np.hsplit(bins,[i]) # weights

    #     # finding means and variances
    #     m1,m2 = np.sum(p1*b1)/q1, np.sum(p2*b2)/q2
    #     v1,v2 = np.sum(((b1-m1)**2)*p1)/q1,np.sum(((b2-m2)**2)*p2)/q2

    #     # calculates the minimization function
    #     fn = v1*q1 + v2*q2
    #     if fn < fn_min:
    #         fn_min = fn
    #         thresh = i

    # find otsu's threshold value with OpenCV function
    ret, otsu = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return ret, otsu

def display_image(img, label='Image'):
    cv2.imshow(label, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def bin_img(img, thresh):    
    ret,thresh1 = cv2.threshold(img,thresh,255,cv2.THRESH_BINARY)
    return thresh1

def main():
    file = sys.argv[1]
    img = cv2.imread(file,0)
    thresh, img_new = calculate_otsu_thresh(img)
    new_img2 = bin_img(img, thresh*1.05)
    display_image(img_new)
    display_image(new_img2)

if __name__ == '__main__':
    main()