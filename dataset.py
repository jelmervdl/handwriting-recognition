from __future__ import division

import sys
import os
from toolbox import wordio
import cv2
import otsu
from collections import defaultdict, Counter

class Page:
    def __init__(self, lines, image_path, words_path):
        self.lines = lines
        self.image_path = image_path
        self.words_path = words_path
        self._image = None
        self._mask = None

    def __str__(self):
        return os.path.basename(self.image_path)

    def __repr__(self):
        return "Page<{words_path} {image_path}>".format(words_path=self.words_path, image_path=self.image_path)

    def __enter__(self):
        if self._image is None:
            self.load()
        return self

    def __exit__(self, *exc_info):
        self.unload()

    @property
    def slice(self):
        '''A bit pointless, I know, since the slice of a page is the whole image, but this causes a page to behave exactly as a Word or a Character.'''
        return (slice(0, None), slice(0, None))

    @property
    def image(self):
        if self._image is None:
            self.load()
        return self._image

    @property
    def mask(self):
        if self._mask is None:
            self.load()
        return self._mask
    
    def load(self):
        self._image = cv2.imread(self.image_path, 0)
        if self._image is None:
            raise Exception("Cannot find image {} for page {}".format(self.image_path, repr(self)))
        self.threshold, self._mask = otsu.calculate_otsu_thresh(self._image)
        cv2.bitwise_not(self._mask, self._mask)

    def unload(self):
        # save a bit of memory
        self._image = None
        self._mask = None

    def save(self, path=None):
        return wordio.save(self.lines, path if path is not None else self.words_path)

    @property
    def shape(self):
        return self.image.shape

    @property
    def width(self):
        return self.image.shape[1]

    @property
    def height(self):
        return self.image.shape[0]

    @property
    def words(self):
        for line in self.lines:
            for word in line:
                yield word

    def __len__(self):
        return sum(len(line) for line in self.lines)

    @property
    def characters(self):
        for word in self.words:
            for character in word.characters:
                yield character, word


class Dataset:
    def __init__(self, image_dir, words_dir, image_filename_format="{}.jpg"):
        self.image_dir = image_dir
        self.words_dir = words_dir
        self.image_filename_format = image_filename_format

    def _words_files(self):
        for filename in os.listdir(self.words_dir):
            if filename.endswith('.words'):
                yield os.path.join(self.words_dir, filename)

    def _parse_words_file(self, filename):
        with file(filename, 'rb') as words_file:
            yield wordio.read(words_file)

    @property
    def pages(self):
        for words_path in self._words_files(): 
            for lines, image_filename in self._parse_words_file(words_path):
                image_path = os.path.join(self.image_dir, self.image_filename_format.format(image_filename))
                if not os.path.exists(image_path):
                    basename, ext = os.path.splitext(os.path.basename(words_path))
                    image_path = os.path.join(self.image_dir, self.image_filename_format.format(basename))
                yield Page(lines, image_path, words_path)

    @property
    def words(self):
        for page in self.pages:
            for word in page.words:
                yield word, page


class CombinedDataset:
    def __init__(self, datasets):
        self.datasets = datasets

    @property
    def pages(self):
        for dataset in self.datasets:
            for page in dataset.pages:
                yield page

    @property
    def words(self):
        for dataset in self.datasets:
            for word, page in dataset.words:
                yield word, page


class Lexicon(defaultdict):
    def __init__(self):
        super(Lexicon, self).__init__(int)

    @staticmethod
    def load(path):
        lexicon = Lexicon()
        with open(path) as f:
            for line in f:
                word, frequency = line.rsplit(' ', 2)
                lexicon[word] = int(frequency)
        return lexicon

    @staticmethod
    def extract(dataset):
        lexicon = Lexicon()
        for word, page in dataset.words:
            lexicon[word.text] += 1
        return lexicon


def ngrams(elements, n):
    return zip(*[elements[i:] for i in range(n)])


def cut_words_from_dataset(dataset, output_path):
    word_counter = Counter()
    for page in dataset.pages:
        for line in page.lines:
            for word in line:
                word_counter[word.text] += 1
                word_filename = os.path.join(output_path, 'words', word.text, "{}.png".format(word_counter[word.text]))
                if not os.path.exists(os.path.dirname(word_filename)):
                    os.makedirs(os.path.dirname(word_filename))
                cv2.imwrite(word_filename, page.image[word.slice])
        page.unload()


def cut_letters_from_dataset(dataset, output_path):
    letter_counter = Counter()
    for page in dataset.pages:
        for word in page.words:
            for character in word.characters:
                letter_counter[character.text] += 1
                char_filename = os.path.join(output_path, 'characters', character.text, "{}.png".format(letter_counter[character.text]))
                if not os.path.exists(os.path.dirname(char_filename)):
                    os.makedirs(os.path.dirname(char_filename))
                cv2.imwrite(char_filename, page.image[character.slice])
        page.unload()


def count_character_ngrams(dataset):
    counter = Counter()
    for n in range(1, 5):
        for word, page in dataset.words:
            for ngram in ngrams(map(lambda char: char.text, word.characters), n):
                counter[ngram] += 1
    return counter


def default_dataset():
    return CombinedDataset([
        Dataset('data/pages/KNMP/','data/charannotations/KNMP/'),
        Dataset('data/pages/Stanford/','data/charannotations/Stanford/')
    ])


def split_pages(dataset):
    pages = list(dataset.pages)

    train_pages = list()
    test_pages = list()

    for n, page in enumerate(pages):
        if n % 8 == 0:
            test_pages.append(page)
        else:
            train_pages.append(page)

    return train_pages, test_pages
