from dataset import default_dataset
from flask import request, Flask, send_file, Response, abort
import cv2
import os
import sys
import base64
import urllib

def imgtobase64(img):
    buffer = cv2.imencode('.jpg', img)[1]
    return "data:image/jpeg;base64," + base64.encodestring(buffer)

dataset = default_dataset()

app = Flask(__name__)
app.debug = True

index = list()

for page in dataset.pages:
        for word in page.words:
            for character in word.characters:
                if character.text.lower() == 's':
                    index.append((character, word, page))

@app.route('/', methods=['GET', 'POST'])
def app_index():
    if len(request.form):
        for i in request.form.keys():
            character, word, page = index[int(i)]
            index[int(i)] = None
            character.text = 'f'
            updated_word_text = "".join(char.text for char in word.characters)
            print("Updating {} to {}".format(word.text, updated_word_text))
            word.text = updated_word_text
            page.save()
        
    while True:
        try:
            idx = index.index(None)
            del index[idx]
        except ValueError:
            break


    def generate():
        yield '''
        <form method="post">
            <table>
        '''
        
        current_page = None
        for i, (character, word, page) in enumerate(index):
            try:
                if current_page != page:
                    if current_page is not None:
                        current_page.unload()
                    current_page = page
                    current_page.load()

                yield '''
                    <tr>
                        <td><input type="checkbox" id="c{i}" name="{i}"></td>
                        <td><label for="c{i}"><img src="{render_char}"></label></td>
                        <td><img src="{render_word}"></td>
                    </tr>
                '''.format(i=i,
                        render_char=imgtobase64(current_page.image[character.slice]),
                        render_word=imgtobase64(current_page.image[word.slice]))
            except:
                pass
    
        yield '''
            </table>
            <button type="submit">Mark as f</button>
        </form>
        '''

    return Response(generate(), mimetype='text/html')

app.run(port=5002)
