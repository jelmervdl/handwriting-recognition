from dataset import default_dataset
from collections import Counter
import re

dataset = default_dataset()

s_counter = 0
f_counter = 0

for page in dataset.pages:
	for word in page.words:
		for character in word.characters:
			if character.text.lower() == 's':
				s_counter += 1
			elif character.text.lower() == 'f':
				f_counter += 1

print("S: {}\nF: {}".format(s_counter, f_counter))