from __future__ import division

from collections import Counter, defaultdict
from dump_reader import load_dump
from flask import Flask, send_file, Response, abort
import numpy as np
import cv2
import os
import sys
import base64
import urllib

def imgtobase64(img):
    buffer = cv2.imencode('.jpg', img)[1]
    return "data:image/jpeg;base64," + base64.encodestring(buffer)

def webservice(folder):
    app = Flask(__name__)
    app.debug = True

    def header(title=''):
        return '''
            <html>
                <head>
                    <title>{title}</title>
                    <style>
                        tr.correct td {{
                            background: rgb(200, 255, 200);
                        }}
                        html, body {{
                            font: 12px/16px sans-serif;
                        }}
                        table {{
                            width: 100%;
                        }}
                        tr.correct td {{
                            background: rgb(200, 255, 200);
                        }}
                        th {{
                            text-align: left;
                        }}
                        .character {{
                            border: 1px solid red;
                        }}
                    </style>
                </head>
                <body>
        '''.format(title=title)

    def footer():
        return '''
                </body>
            </html>
        '''

    @app.route('/')
    def app_show_index():
        dump_names = [os.path.splitext(filename)[0] for filename in os.listdir(folder) if filename.endswith('.pz')]
        dump_names.sort(key=lambda name: int(name.split('_')[0]))
        return header() + '<ul>{}</ul>'.format("\n".join('<li><a href="/{0}">{0}</a>'.format(name) for name in dump_names)) + footer()

    @app.route('/detail/<dump>')
    def app_show_dump(dump):
        rows = list()
        solution = load_dump("{}/{}.pz".format(folder, dump))
        page = solution['page']
        for idx, solution in enumerate(solution['solutions']):
            rows.append('''
                <tr>
                    <th>{idx}</th>
                    <td>{confidence}</td>
                    <td>{solution}</td>
                </tr>
            '''.format(
                    idx = idx,
                    confidence=solution.confidence,
                    solution=solution))
            for character in solution.characters:
                rows.append('''
                <tr>
                    <th><img src="{encoded_image_url}"></th>
                    <td>{confidence}</td>
                    <td>{solution}</td>
                </tr>
            '''.format(
                    encoded_image_url=imgtobase64(page.image[character.slice]),
                    confidence=character.confidence,
                    solution=character))

        return header() + '''
            <table>
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Confidence</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        '''.format(rows="\n".join(rows)) + footer()

    def render_character(character, word, page):
        return '''<div title="{text} ({confidence})"
            class="character"
            style="
                position: absolute;
                top:{top}px;
                left:{left}px;
                width:{width}px;
                height:{height}px;"></div>
        '''.format(
            text=character.text,
            confidence=character.confidence,
            top=character.top - word.top,
            left=character.left - word.left,
            width=character.right - character.left,
            height=character.bottom - character.top)

    def render_separate_characters(word, page):
        return "\n".join(['<img src="{render}" width="{width}" height="{height}">'.format(
            render=imgtobase64(page.image[character.slice]),
            width=character.shape[0],
            height=character.shape[1]) for character in word.characters])

    def render_word(word, page):
        return '''<div
            style="
                position: relative;
                width: {width}px;
                height: {height}px;
            ">
                <img src="{page}" style="position: absolute; top: 0; left: 0; opacity: 0.25;">
                <img src="{image}" style="position: absolute; top: 0; left: 0">
                {characters}
            </div>'''.format(
                width=max(word.right - word.left, word.characters[-1].right - word.characters[0].left),
                height=word.bottom - word.top,
                image=imgtobase64(page.image[word.slice]),
                page=imgtobase64(page.image[word.slice[0], slice(word.characters[0].slice[1].start, word.characters[-1].slice[1].stop)]),
                characters="\n".join(render_character(character, word, page) for character in word.characters))

    @app.route('/<dump_name>')
    def app_show_dump_detail(dump_name):
        dump_names = [os.path.splitext(filename)[0] for filename in os.listdir(folder) if filename.endswith('.pz')]
        dump_names.sort(key=lambda name: int(name.split('_')[0]))
        dump = load_dump("{}/{}.pz".format(folder, dump_name))
        page = dump['page']
        def generate():
            yield header(dump_name)

            yield '''
                <table>
                    <tr>
                        <td><a href="{prev_dump}">{prev_dump}</a></td>
                        <td align="center">{dump_name}</td>
                        <td align="right"><a href="{next_dump}">{next_dump}</a></td>
                    </tr>
                </table>
                
                <table>
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Characters</th>
                            <th>Word</th>
                            <th>Confidence</th>
                        </tr>
                    </thead>
                    <tbody>
            '''.format(
                dump_name=dump_name,
                next_dump=None, #dump_names[dump_names.index(dump_name) + 1],
                prev_dump=None) #dump_names[dump_names.index(dump_name) - 1])

            yield '''
                <tr>
                    <td>{render}</td>
                    <td>{render_characters}</td>
                    <td>{word.text}</td>
                    <td>Goal</td>
                </tr>
                <tr>
                    <td colspan="3"><hr></td>
                </tr>
            '''.format(
                render=render_word(dump['word'], page),
                render_characters=render_separate_characters(dump['word'], page),
                word=dump['word'])

            for solution in dump['solutions']:
                yield '''
                    <tr class="{classname}">
                        <td>{render}</td>
                        <td>{render_characters}</td>
                        <td>{word.text}</td>
                        <td>{word.confidence}</td>
                    </tr>
                '''.format(
                    render=render_word(solution, page),
                    render_characters=render_separate_characters(solution, page),
                    word=solution,
                    classname="correct" if solution.text == dump['word'].text else "incorrect")

            yield '''
                </tbody>
            </table>
            '''
            yield footer()
            page.unload()
        return Response(generate(), mimetype='text/html')

    return app

if __name__ == '__main__':
    webservice("dumps").run()
